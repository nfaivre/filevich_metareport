clear all %#ok<CLALL>

global cfg;
global dev;
global DATA;

rootPath        = '~/git/noReport_metacognition/';
cfg.dataPath    = '/Data_temp/';
functionsPath   = '/functions/';
mkdir([rootPath cfg.dataPath])
addpath(rootPath)
addpath([rootPath functionsPath])

cfg.debug = 1;

commandwindow;

%% SET UP
%% load parameters from previous codes

dlg = inputdlg({'Pseudonym', 'Staircase', 'Map colour to eye: 1 or 2', ...
    'Screen height (cm)', 'Screen height (pixels)', 'Practice? (Default:1/0)', ... 
    'Luminance red after callibration', 'Luminance green after callibration'},'Input');
DATA.info.pseudonym    = dlg{1};
cfg.doStaircase        = str2double(dlg{2}); % TODO decide if you run a first training staircase (1) or the real expe (0)
cfg.color2EyeMap       = str2double(dlg{3});
dev.screenHeightCm     = str2double(dlg{4});
dev.screenHeightPixels = str2double(dlg{5});
cfg.practice           = str2double(dlg{6});
cfg.redLuminance       = str2double(dlg{7});
cfg.greenLuminance     = str2double(dlg{8});
if isempty(DATA.info.pseudonym), DATA.info.pseudonym = 'test'; end
if isnan(cfg.doStaircase),       cfg.doStaircase     = 0;      end
if isnan(cfg.color2EyeMap),      cfg.color2EyeMap    = 1;      end          %1: green is left. 2: green is right
if isnan(dev.screenHeightCm),    dev.screenHeightCm  = 33.6;   end
if isnan(dev.screenHeightPixels),dev.screenHeightPixels = 1440;end          %Defaults to large Dell Screens
if isnan(cfg.practice),          cfg.practice        = 1;      end
if isnan(cfg.redLuminance),      cfg.redLuminance    = 1;      end
if isnan(cfg.greenLuminance),    cfg.greenLuminance  = 1;      end
% But do not do practice if you're staircasing!
if cfg.doStaircase,              cfg.practice        = 0;      end
cfg.dataPathSubj        = [rootPath cfg.dataPath DATA.info.pseudonym];

%collect only what you need from alignCenters result, then discard
%everything else
alignmentDATA            = load([cfg.dataPathSubj '/noReport_alignCentres.mat']);
DATA.centreLeftAdjusted  = alignmentDATA.DATA.align.centreLeftAdjusted;
DATA.centreRightAdjusted = alignmentDATA.DATA.align.centreRightAdjusted;
DATA.info.age            = alignmentDATA.DATA.info.age;
DATA.info.sex            = alignmentDATA.DATA.info.sex;
clear alignmentDATA     

%% Config stimuli
if cfg.debug    
    Screen('Preference', 'SkipSyncTests', 1);
    PsychDebugWindowConfiguration;
else
    Screen('Preference', 'SkipSyncTests', 0);
end
cfg.viewingDistance    = 60;    % Distance between monitor and participant in cm
cfg.verticalPixNr      = dev.screenHeightPixels;  % Vertical resolution of the monitor (in pixels)
cfg.stimSizeDegrees    = 10;    % Desired value

% Calculate number of degrees per single pixel. Should be ~0.03
degreesPerPixel     = atan2d(.5 * dev.screenHeightCm, cfg.viewingDistance) / (.5 * cfg.verticalPixNr);
% Calculate the size of the stimulus in degrees
cfg.stimSizePixels = round(cfg.stimSizeDegrees / degreesPerPixel);

cfg.cyclesPerDegree = .27;  %From Fraessle et al
% createGratingTexture.m requires cycles/pixel: By default 0.05 cycles per pixel.
cfg.cyclesPerPixel  = cfg.cyclesPerDegree * degreesPerPixel;

% createGratingTexture.m requires speed of grating in cycles per second: 1 cycle per second by default.
cfg.degreesPerSec    = 22.3; %From Fraessle et al
cfg.cyclesPerSecond  = cfg.degreesPerSec * cfg.cyclesPerDegree;
cfg.tiltFromVertical = 0;              % If we need to counterbalance the motion direction between subjects, assign 180 or 0 degress alternatively
cfg.drawmask         = 0; %no gaussian mask over grating %TODO if we don't want this, declutter the corresponding bits of code
cfg.redLuminance     = .75;     %starting values
cfg.greenLuminance   = .75;
cfg.luminanceStep    = .05;
DATA.continuousReport(1,1).redLuminance   = cfg.redLuminance;   %dynamics will be recorded here
DATA.continuousReport(1,1).greenLuminance = cfg.greenLuminance;

%% Config instruction (report/no report) instruction cue
cfg.conditionCuePositionY = -50;       %position in pixels from the top of the gratings where the condition cues will be shown  
cfg.conditionCuePositionX = 10;        %we show two little circles. this is half the distance in pixels between them
cfg.conditionCueSize      = 10;        %size of each of the circles that cue the condition (report or no report)
cfg.conditionCueDuration  = .3;        %time before the grating stimuli are displayed but instructions (report/no report) are displayed


%% config trials
if cfg.practice
    cfg.nblocks                  = 2;
    cfg.ntrials                  = 10; %trials per block. *Make this an even number*
    cfg.trialDuration            = 5 * ones(1,cfg.ntrials);
else
    cfg.nblocks                  = 2;                                       %TODO: Decide how many
    cfg.ntrials                  = 20; %trials per block. *Make this an even number*
    cfg.trialDuration            = 5 * ones(1,cfg.ntrials);
end
cfg.attentionProbeProportion = .5;
cfg.attentionProbesPerTrial  = 2;   %TODO: agree?
cfg.attentionProbeNumber     = floor(cfg.attentionProbeProportion * cfg.ntrials);
cfg.attentionProbeID         = 'LT';                                        %so 'L' is 1 and T is 2 
cfg.reportTrialsProportion   = .5;
cfg.type1questionProportion  = .5; %continuous report and type1 question will be fully crossed for .25 each
cfg.reportTrialsNumber       = floor(cfg.reportTrialsProportion * cfg.ntrials);
cfg.type1questionNumber      = floor(cfg.type1questionProportion * cfg.ntrials);

%% Define conditions if this is a staircase
% Overwrite a lot of the previous stuff if you are staircasing stimuli luminance 
if cfg.doStaircase
    cfg.nblocks                  = 1;
    cfg.ntrials                  = 20; %But will stop sooner according to decision rule
    cfg.trialDuration            = 8 * ones(1,cfg.ntrials);
    cfg.attentionProbeNumber     = 0;
    cfg.reportTrialsNumber       = cfg.ntrials;
    cfg.type1questionNumber      = 0;
end

%% Config Attentional Probe 
% to measure possible attentional differences between the OKN-only and the continuous report conditions. 
cfg.timeMarginProbe  = .5; %probe-free sec after and before trial onset and offset 
cfg.attentionalProbeDuration = .1;

% These first ones are all for each block
cfg.attentionalProbe    = ones(cfg.attentionProbeNumber,1); 
cfg.attentionalProbe    = padarray(cfg.attentionalProbe, cfg.ntrials-length(cfg.attentionalProbe), 0, 'post')';
cfg.reportTrial         = ones(cfg.reportTrialsNumber,1);
cfg.reportTrial         = padarray(cfg.reportTrial, cfg.ntrials-length(cfg.reportTrial), 0, 'post')';
cfg.type1questionTrial  = ones(cfg.type1questionNumber,1);
cfg.type1questionTrial  = padarray(cfg.type1questionTrial, cfg.ntrials-length(cfg.type1questionTrial));

%% Shuffle conditions independently for each block 
for b = 1:cfg.nblocks
    cfg.attentionalProbe = shuffle(cfg.attentionalProbe);       
    [cfg.reportTrial, shuffleOrder] = shuffle(cfg.reportTrial);              
                                                                                                                        
    DATA.trialDef.attentionalProbe(b,:)         = cfg.attentionalProbe;
    DATA.trialDef.trialDuration(b,:)            = cfg.trialDuration;
    DATA.trialDef.reportTrial(b,:)              = cfg.reportTrial;
    DATA.trialDef.type1questionTrial(b,:)       = cfg.type1questionTrial(shuffleOrder);
end

%% config response buttons
%Set reponse device 
cfg.allInputs = 'gamepad'; %'keyboard';
[cfg, dev] = setKeys4Responses(cfg, dev); 


%% Open display
screens = Screen('Screens');
dev.screenNumber = max(screens);

%short function to get real black, white, grey values. Taken from PTB demo,
%might be totally unnecessary
dev = findColorValues(dev);

cfg.txtCol  = dev.white;

% Open a double buffered fullscreen window and set default background color to gray
[dev.wnd, dev.screenRect] = Screen('OpenWindow', dev.screenNumber, dev.gray);

% get the x,y centre coordinates
dev.centreScreenX       = (dev.screenRect(3) - dev.screenRect(1))/2;
dev.centreScreenY       = (dev.screenRect(4) - dev.screenRect(2))/2;

%% Config VAS for confidence ratings
cfg.maxConfRT          = Inf;            %secs
cfg.VAS.vertical       = 1;  
cfg.nSteps             = 100;           %may be approximate due to rounding of the VASrange/nsteps
cfg.VAS.bandWidth      = 20;                
cfg.VAS.stringLow      = 'Sehr unsicher';
cfg.VAS.stringHigh     = 'Sehr sicher';
cfg.VAS.questionString = 'Confident of dominance?';
cfg.VAS.nBands         = 10;
cfg.VAS.bandLength     = round(dev.centreScreenY/10);     %width of each colour band in the confidence scale
cfg.VAS.colorLight     = dev.white * [1 1 1];
cfg.VAS.colorDark      = dev.black * [1 1 1];


%% Config mouse
dev.mouseId = GetMouseIndices;
dev.mouseId = dev.mouseId(1);






%% EXPERIMENT
%% Display instructions
DrawFormattedText(dev.wnd, cfg.expInstructions, 'center', 'center', dev.white);
Screen('Flip', dev.wnd);

% Wait for start key. Complicated, but works for both keyboard and gamepad 
PsychHID('KbQueueCreate', dev.inputRivalryIndex); 
PsychHID('KbQueueStart', dev.inputRivalryIndex);
pressedKey = 0;
while ~pressedKey
    [pressedKey, firstPress, firstRelease] = PsychHID('KbQueueCheck', dev.inputRivalryIndex);
    if ~ismember(find(firstPress), cfg.startKey)
        pressedKey = 0;
    end
end
PsychHID('KbQueueStop', dev.inputRivalryIndex);
PsychHID('KbQueueFlush', dev.inputRivalryIndex);

%% Display trials 

for b = 1:cfg.nblocks
    for t = 1:cfg.ntrials
            
        %Get times of Poisson-distributed attentional probes
        if DATA.trialDef.attentionalProbe(b,t)
            DATA.trialDef.poissonTimes{b,t} = getPoissonTimes(b, t);
            DATA.trialDef.attentionalProbeShownID{b,t} = round(rand(size(DATA.trialDef.poissonTimes{b,t})) + 1); %rand between 1 and 2
        end

        % Create all textures to display the colour dynamic stimuli with adaptive luminance
        % dynamicGrating is a structure containing several textures.
        if cfg.doStaircase
            dynamicGrating = createGratingTexture(DATA.continuousReport(b,t).redLuminance, DATA.continuousReport(b,t).greenLuminance);
        else
            dynamicGrating = createGratingTexture(cfg.redLuminance, cfg.greenLuminance);
        end
        
        %Show gratings and record responses if required
        DATA = showRivalry(dynamicGrating, b, t, DATA);

        %Type 1 response
        if DATA.trialDef.type1questionTrial(b,t) 
            DATA = collectType1Response(b, t, DATA);
        end
        
        %show confidence VAS
        if ~cfg.doStaircase
            DATA = collectVASjudge_vertical(b, t, DATA);
        end
        
        %calculate dominance - will output result into the structure 
        %DATA.continuousReport
        DATA = calculateDominantPercept(b, t, DATA);
        
        %TODO with eyetracker: calculate dominance and output into the structure 
        %DATA.OKN
                
        %Compare reported dominance (if it exists TODO: OR OKN) to 2AFC (if it exists) 
        DATA = evaluateType1Response(b, t, DATA);
        
        %Staircase 
        if cfg.doStaircase
            DATA = staircaseLuminance(b,t,DATA);
        end
        
        %Go to next trial (self-paced)
        gotoNextTrial(b,t)

    end
    save([cfg.dataPathSubj '/noReport_experiment_block' num2str(b) '_' date '.mat']);
end
PsychHID('KbQueueRelease', dev.inputRivalryIndex);
Screen('CloseAll')

if cfg.practice, suffix = 'Practice'; else suffix = ''; end
if cfg.doStaircase, suffix = 'Staircase'; else suffix = ''; end

if exist([cfg.dataPathSubj '/noReport_experiment' suffix '.mat'], 'file')
    save([cfg.dataPathSubj '/noReport_experiment' suffix datestr(now) '.mat']);
else
    save([cfg.dataPathSubj '/noReport_experiment' suffix '.mat']);
end


if cfg.doStaircase
    %extractfield will only work if this is all one block. It expects an
    %array, not a matrix
    greenLuminance = extractfield(DATA.continuousReport, 'greenLuminance');
    redLuminance   = extractfield(DATA.continuousReport, 'redLuminance');
    figure; hold on
    plot(greenLuminance, 'g-o');
    plot(redLuminance,   'r-o');
    ylabel('Luminance');
    xlabel('Trial nr');
end




