clear all %#ok<CLALL>

global cfg;
global dev;
global DATA;

rootPath        = '~/git/noReport_metacognition/';
cfg.dataPath    = '/Data_temp/';
functionsPath   = '/functions/';
mkdir([rootPath cfg.dataPath])
addpath(rootPath)
addpath([rootPath functionsPath])

cfg.debug = 1;

commandwindow;

%% SET UP
%% load parameters from previous codes

dlg = inputdlg({'Pseudonym', 'Map colour to eye: 1 or 2', ...
    'Screen height (cm)', 'Screen height (pixels)', ...
    'Luminance red after callibration', 'Luminance green after callibration'},'Input');
DATA.info.pseudonym    = dlg{1};
cfg.color2EyeMap       = str2double(dlg{2});
dev.screenHeightCm     = str2double(dlg{3});
dev.screenHeightPixels = str2double(dlg{4});
cfg.redLuminance       = str2double(dlg{5});
cfg.greenLuminance     = str2double(dlg{6});
if isempty(DATA.info.pseudonym), DATA.info.pseudonym = 'test'; end
if isnan(cfg.color2EyeMap),      cfg.color2EyeMap    = 1;      end          %1: green is left. 2: green is right
if isnan(dev.screenHeightCm),    dev.screenHeightCm  = 33.6;   end
if isnan(dev.screenHeightPixels),dev.screenHeightPixels = 1440;end          %Defaults to large Dell Screens
if isnan(cfg.redLuminance),      cfg.redLuminance    = 1;      end
if isnan(cfg.greenLuminance),    cfg.greenLuminance  = 1;      end

cfg.dataPathSubj        = [rootPath cfg.dataPath DATA.info.pseudonym];

%collect only what you need from alignCenters result, then discard
%everything else
alignmentDATA            = load([cfg.dataPathSubj '/noReport_alignCentres.mat']);
DATA.centreLeftAdjusted  = alignmentDATA.DATA.align.centreLeftAdjusted;
DATA.centreRightAdjusted = alignmentDATA.DATA.align.centreRightAdjusted;
DATA.info.age            = alignmentDATA.DATA.info.age;
DATA.info.sex            = alignmentDATA.DATA.info.sex;
clear alignmentDATA     

%% Config stimuli
if cfg.debug    
    Screen('Preference', 'SkipSyncTests', 1);
    PsychDebugWindowConfiguration;
else
    Screen('Preference', 'SkipSyncTests', 0);
end
cfg.viewingDistance    = 60;    % Distance between monitor and participant in cm
cfg.verticalPixNr      = dev.screenHeightPixels;  % Vertical resolution of the monitor (in pixels)
cfg.stimSizeDegrees    = 10;    % Desired value

% Calculate number of degrees per single pixel. Should be ~0.03
degreesPerPixel     = atan2d(.5 * dev.screenHeightCm, cfg.viewingDistance) / (.5 * cfg.verticalPixNr);
% Calculate the size of the stimulus in degrees
cfg.stimSizePixels = round(cfg.stimSizeDegrees / degreesPerPixel);

cfg.cyclesPerDegree = .27;  %From Fraessle et al
% createGratingTexture.m requires cycles/pixel: By default 0.05 cycles per pixel.
cfg.cyclesPerPixel  = cfg.cyclesPerDegree * degreesPerPixel;

% createGratingTexture.m requires speed of grating in cycles per second: 1 cycle per second by default.
cfg.degreesPerSec    = 22.3; %From Fraessle et al
cfg.cyclesPerSecond  = cfg.degreesPerSec * cfg.cyclesPerDegree;
cfg.tiltFromVertical = 0;              % If we need to counterbalance the motion direction between subjects, assign 180 or 0 degress alternatively
cfg.drawmask         = 0; %no gaussian mask over grating %TODO if we don't want this, declutter the corresponding bits of code
cfg.conditionCuePositionY = -50;       %position in pixels from the top of the gratings where the condition cues will be shown  
cfg.conditionCuePositionX = 10;        %we show two little circles. this is half the distance in pixels between them
cfg.conditionCueSize      = 10;        %size of each of the circles that cue the condition (report or no report)
cfg.conditionCueDuration  = .3;        %time before the grating stimuli are displayed but instructions (report/no report) are displayed

%% config trials
cfg.nblocks                  = 1;
cfg.ntrials                  = 1; 
cfg.trialDuration            = 30 * ones(1,cfg.ntrials);
cfg.attentionProbeProportion = 1;
cfg.attentionProbesPerTrial  = round(cfg.trialDuration/3);   
cfg.attentionProbeNumber     = floor(cfg.attentionProbeProportion * cfg.ntrials);
cfg.attentionProbeID         = 'LT';                                        %so 'L' is 1 and T is 2 
cfg.reportTrialsProportion   = 1;
cfg.type1questionProportion  = 1; 
cfg.reportTrialsNumber       = floor(cfg.reportTrialsProportion * cfg.ntrials);
cfg.type1questionNumber      = floor(cfg.type1questionProportion * cfg.ntrials);

% Attentional Probe to measure possible attentional differences between the
% OKN-only and the continuous report conditions. 
cfg.timeMarginProbe  = .5; %500 msec after and before trial onset and offset there will be no probes
cfg.attentionalProbeDuration = .1;


%% config response buttons
%Set reponse device 
cfg.allInputs = 'gamepad'; %'gamepad' or 'keyboard';
[cfg, dev, allDevs] = setKeys4Responses(cfg, dev); 

% regardless of actual input from subject, query keyboard to see what to show 
dev.inputKeyboardIndex = structfind(allDevs, 'usageName', 'Keyboard');

%% Open display
screens = Screen('Screens');
dev.screenNumber = max(screens);

%short function to get real black, white, grey values. Taken from PTB demo,
%might be totally unnecessary
dev = findColorValues(dev);

cfg.txtCol  = dev.white;

% Open a double buffered fullscreen window and set default background color to gray
[dev.wnd, dev.screenRect] = Screen('OpenWindow', dev.screenNumber, dev.gray);

% get the x,y centre coordinates
dev.centreScreenX       = (dev.screenRect(3) - dev.screenRect(1))/2;
dev.centreScreenY       = (dev.screenRect(4) - dev.screenRect(2))/2;


%% EXPERIMENT
%% Display instructions
DrawFormattedText(dev.wnd, cfg.expInstructions_startOnly, 'center', 'center', dev.white);
Screen('Flip', dev.wnd);

% Wait for start key. Complicated, but works for both keyboard and gamepad 
PsychHID('KbQueueCreate', dev.inputRivalryIndex); 
PsychHID('KbQueueStart', dev.inputRivalryIndex);
pressedKey = 0;
while ~pressedKey
    [pressedKey, firstPress, firstRelease] = PsychHID('KbQueueCheck', dev.inputRivalryIndex);
    if ~ismember(find(firstPress), cfg.startKey)
        pressedKey = 0;
    end
end
PsychHID('KbQueueStop', dev.inputRivalryIndex);
PsychHID('KbQueueFlush', dev.inputRivalryIndex);

%% Display trials 
% Create all textures to display the colour (or greyscale) dynamic stimuli.
% dynamicGrating is a structure containing quite a few different textures. 
dynamicGrating = createGratingTexture(cfg.redLuminance, cfg.greenLuminance);

PsychHID('KbQueueCreate', dev.inputKeyboardIndex); 
for b = 1:cfg.nblocks
    for t = 1:cfg.ntrials

        %Get times of Poisson-distributed attentional probes
        DATA.trialDef.trialDuration(b,t) = cfg.trialDuration; %set this so you don't modify the other function
        DATA.trialDef.poissonTimes{b,t} = getPoissonTimes(b, t);
        DATA.trialDef.attentionalProbeShownID{b,t} = round(rand(size(DATA.trialDef.poissonTimes{b,t})) + 1); %rand between 1 and 2
        
        %Show gratings and record responses if required
        DATA = showRivalry_explainTask(dynamicGrating, b, t, DATA);

    end
end
PsychHID('KbQueueRelease', dev.inputKeyboardIndex);
Screen('CloseAll')




