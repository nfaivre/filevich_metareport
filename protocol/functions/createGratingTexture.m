function [dynamicGrating] = createGratingTexture(redLuminance, greenLuminance)

% all this also taken from DriftDemo2

global cfg;
global dev;

% A bit silly to revert back to less informative names but this way I
% don't need to change the code from the demo, which was otherwise
% excellent
f       = cfg.cyclesPerPixel;
gray    = dev.gray;
white   = dev.white;
inc     = dev.inc;


if cfg.drawmask
    % Enable alpha blending for proper combination of the gaussian aperture
    % with the drifting sine grating:
    Screen('BlendFunction', w, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
end

% Calculate parameters of the grating:

% First we compute pixels per cycle, rounded up to full pixels, as we
% need this to create a grating of proper size below:
p = ceil(1/f);

% Also need frequency in radians:
fr = f * 2 * pi;

% Define texture as half-Size of the grating image.
texsize = cfg.stimSizePixels  / 2;

% This is the visible size of the grating. It is twice the half-width
% of the texture plus one pixel to make sure it has an odd number of
% pixels and is therefore symmetric around the center of the texture:
visiblesize = 2 * texsize + 1;

% Create one single static grating image:
%
% We only need a texture with a single row of pixels(i.e. 1 pixel in height) to
% define the whole grating! If the 'srcRect' in the 'Drawtexture' call
% below is "higher" than that (i.e. visibleSize >> 1), the GPU will
% automatically replicate pixel rows. This 1 pixel height saves memory
% and memory bandwith, ie. it is potentially faster on some GPUs.
%
% However it does need 2 * texsize + p columns, i.e. the visible size
% of the grating extended by the length of 1 period (repetition) of the
% sine-wave in pixels 'p':
x = meshgrid(-texsize:texsize + p, 1);

% Compute actual cosine grating:
grating = gray + inc*cos(fr*x);

% Store 1-D single row grating in texture:
gratingtex      = Screen('MakeTexture', dev.wnd, grating);
gratingtexRed   = Screen('MakeTexture', dev.wnd, cat(3, redLuminance * grating, zeros(size(grating)), zeros(size(grating))));
gratingtexGreen = Screen('MakeTexture', dev.wnd, cat(3, zeros(size(grating)), greenLuminance * grating, zeros(size(grating))));

% Create a single gaussian transparency mask and store it to a texture:
% The mask must have the same size as the visible size of the grating
% to fully cover it. Here we must define it in 2 dimensions and can't
% get easily away with one single row of pixels.
%
% We create a  two-layer texture: One unused luminance channel which we
% just fill with the same color as the background color of the screen
% 'gray'. The transparency (aka alpha) channel is filled with a
% gaussian (exp()) aperture mask:
mask = ones(2*texsize+1, 2*texsize+1, 2) * gray;
[x,y] = meshgrid(-1*texsize:1*texsize,-1*texsize:1*texsize);
mask(:, :, 2) = white * (1 - exp(-((x/90).^2)-((y/90).^2)));
masktex = Screen('MakeTexture', dev.wnd, mask);

% Query maximum useable priorityLevel on this system:
priorityLevel = MaxPriority(dev.wnd); % %#ok<NASGU>

% We don't use Priority() in order to not accidentally overload older
% machines that can't handle a redraw every 40 ms. If your machine is
% fast enough, uncomment this to get more accurate timing.
Priority(priorityLevel);

% Definition of the drawn rectangle on the screen:
% Compute it to  be the visible size of the grating, centered on the
% screen:
dstRect = [0 0 visiblesize visiblesize];
dstRect = CenterRect(dstRect, dev.screenRect);

% Return all in one structure
dynamicGrating.gratingtex      = gratingtex;
dynamicGrating.gratingtexRed   = gratingtexRed;
dynamicGrating.gratingtexGreen = gratingtexGreen;
dynamicGrating.masktex         = masktex;
dynamicGrating.dstRect         = dstRect;
dynamicGrating.visiblesize     = visiblesize;


return