%callibrate gamepad
gamepadIndex = Gamepad('GetNumGamepads');
axisState = Gamepad('GetAxis', gamepadIndex, 1)

hatState = Gamepad('GetHat', gamepadIndex, 1)


for b = 1:12
    button(b) = Gamepad('GetButton', gamepadIndex, b);
end
find(button)












%remap from the gamepad range to the Y mouse range. Step necessary
%for drawing. Will then be undone to get the confidence value

% Attempts to use the binary part of the gamepad output properly
%thumbstickValue = thumbstickValue - 128;
thumbstickValue_bin = dec2bin(thumbstickValue, 15);
thumbstickValue_8bit = bin2dec(thumbstickValue_bin(1:8));

%thumbstickValue = thumbstickValue + 129;
thumbstickValue_bin = dec2bin(abs(thumbstickValue+1), 15);
thumbstickValue_8bit = -bin2dec(thumbstickValue_bin(1:8));

%mouseY = thumbstickValue_8bit - dev.gamepadYmin_8bit / (dev.gamepadYmax_8bit - dev.gamepadYmin_8bit) ...
%    * (VASrange(2) - VASrange(1)) + VASrange(1);


cfg.VASaxisIndex            = 4;
dev.gamepadYmax             = 32767;
dev.gamepadYmax_bin         = dec2bin(dev.gamepadYmax);
dev.gamepadYmax_8bit        = bin2dec(dev.gamepadYmax_bin(1:8));
dev.gamepadYmin             = -32768;
dev.gamepadYmin_bin         = dec2bin(abs(dev.gamepadYmin+1));
dev.gamepadYmin_8bit        = -bin2dec(dev.gamepadYmin_bin(1:8));
dev.gamepadYnegZero         = -129;
dev.gamepadYposZero         = 128;
