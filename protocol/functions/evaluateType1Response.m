function DATA = evaluateType1Response(b, t, DATA)

global cfg;

%compare reported dominance to 2AFC, if both existed

if ~isempty(DATA.continuousReport(b,t).keyIDpress) && DATA.trialDef.type1questionTrial(b,t)
    
    [~, dominantColour] = max([DATA.continuousReport(b,t).reportedDominance.greenSecs, DATA.continuousReport(b,t).reportedDominance.redSecs]);
    
    if dominantColour == 1  %Green
        DATA.continuousReport(b,t).dominantColour = 'g';
        if DATA.type1Question(b,t).responseKeyID == cfg.type1ResponseKeyGreen
            DATA.type1Question(b,t).correct = 1;
        elseif DATA.type1Question(b,t).responseKeyID == cfg.type1ResponseKeyRed
            DATA.type1Question(b,t).correct = 0;
        else
            DATA.type1Question(b,t).correct = NaN;
        end
    elseif dominantColour == 2  %Red
        DATA.continuousReport(b,t).dominantColour = 'r';
        if DATA.type1Question(b,t).responseKeyID == cfg.type1ResponseKeyGreen
            DATA.type1Question(b,t).correct = 0;
        elseif DATA.type1Question(b,t).responseKeyID == cfg.type1ResponseKeyRed
            DATA.type1Question(b,t).correct = 1;
        else
            DATA.type1Question(b,t).correct = NaN;
        end
    end
    
    if DATA.type1Question(b,t).responseKeyID == cfg.type1ResponseKeyGreen
        DATA.type1Question(b,t).dominantColour = 'g';
    elseif DATA.type1Question(b,t).responseKeyID == cfg.type1ResponseKeyRed
        DATA.type1Question(b,t).dominantColour = 'r';
    else
        DATA.type1Question(b,t).dominantColour = 'x';
    end
    
    
end