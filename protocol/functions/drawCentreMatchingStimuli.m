
function drawCentreMatchingStimuli(centreLeftAdjusted, centreRightAdjusted)

global dev;
global cfg;

%% Define stimuli

smallSq_side    = 5;
smallSq_rect    = [0 0 smallSq_side smallSq_side];
nSmallSqinBig   = 10;               %number of small squares forming the big one
bigSq_side      = nSmallSqinBig * smallSq_side;
penWidth        = 1;

bigSq_x = [(-bigSq_side : smallSq_side : bigSq_side),...
    bigSq_side * ones(1,nSmallSqinBig*2),...
    (bigSq_side : -smallSq_side : -bigSq_side),...
    -bigSq_side * ones(1,nSmallSqinBig*2)];

bigSq_y = [-bigSq_side * ones(1,nSmallSqinBig*2),...
    (-bigSq_side : smallSq_side : bigSq_side),...
    bigSq_side * ones(1,nSmallSqinBig*2),...
    (bigSq_side : -smallSq_side : -bigSq_side)];

for sq = 1:length(bigSq_x)
    
    %same colour in left and right, should be easier to match
    if mod(sq,2); smallSqColour = dev.white; else smallSqColour = dev.grey; end
    
    %left square
    smallSq_centre = CenterRectOnPoint(smallSq_rect, centreLeftAdjusted + bigSq_x(sq), dev.centreScreenY + bigSq_y(sq));
    Screen('FrameRect', dev.wnd, smallSqColour,  smallSq_centre, penWidth);
    
    %right square
    smallSq_centre = CenterRectOnPoint(smallSq_rect, centreRightAdjusted + bigSq_x(sq), dev.centreScreenY + bigSq_y(sq));
    Screen('FrameRect', dev.wnd, smallSqColour,  smallSq_centre, penWidth);
    
end

% Draw little circles in the middle of the squares: left and right
rectsForOvals = cat(2,[centreLeftAdjusted  - cfg.midPointSize; dev.centreScreenY + cfg.midPointSize; ...
                       centreLeftAdjusted  + cfg.midPointSize; dev.centreScreenY - cfg.midPointSize], ...
                      [centreRightAdjusted - cfg.midPointSize; dev.centreScreenY + cfg.midPointSize; ...
                       centreRightAdjusted + cfg.midPointSize; dev.centreScreenY - cfg.midPointSize]);
Screen('FillOval', dev.wnd, dev.white, rectsForOvals);
Screen('Flip', dev.wnd);


end
