
function DATA = collectVASjudge_vertical(block, trial, DATA)

% 'Plugin' function to display and record responses to a visual analog
% scale. 
% 
% You'll have to set some configuration parameters in the function calling
% this one. They are:
% 
% Get mouseId
% dev.mouseId = GetMouseIndices;
% dev.mouseId = dev.mouseId(1);
% 
% [dev.wnd, dev.rect] = Screen('OpenWindow', screen, ... your OpenScreen
% command
%
% cfg.txtCol  = 255 * [1 1 1];
% dev.xCenter = (dev.rect(3) - dev.rect(1))/2 + dev.rect(1);
% dev.yCenter = (dev.rect(4) - dev.rect(2))/2 + dev.rect(2);
% 
% 
% cfg.maxConfRT          = Inf;            %secs
% cfg.VAS.bandWidth      = 20;
% cfg.VAS.stringLow      = 'Sehr unsicher';
% cfg.VAS.stringHigh     = 'Sehr sicher';
% cfg.VAS.questionString = 'Wie sicher?';
% cfg.VAS.nBands         = 10;
% cfg.VAS.bandLength     = round(dev.xCenter/7);     %width of each colour band in the confidence scale
% cfg.VAS.colorLight     = round(1/2 * 255 * [1 1 1]);
% cfg.VAS.colorDark      = 255 * [1 1 1];
%
% Data are stored in the DATA.VAS

global cfg;
global dev;

%draw display intially to get VASrange
%things are centered on x on the DATA.centreRightAdjusted point. This would
%normally be dev.centreScreenX but we need to show it to one side only
VASrange = drawVAS(DATA);
cfg.mouseStep = round(VASrange(2) - VASrange(1))/cfg.nSteps;

%VASrange  = [dev.xCenter-100, dev.xCenter+100];
initialy  = VASrange(1) + (VASrange(2) - VASrange(1)) * rand;
initialy  = round(initialy);
hiddenx   = 0;

if cfg.debug
    [initialx] = GetMouse;
    SetMouse(initialx, initialy, dev.wnd, dev.mouseId);
else
    SetMouse(hiddenx, initialy, dev.wnd, dev.mouseId); 
end
DATA.VAS(block,trial).mousePosOrig = (initialy - VASrange(1))/range(VASrange);
DATA.VAS(block,trial).onset = GetSecs;


%% Initialize

counter       = 1;
mousetrail_y  = [];
movementBegun = 0;
confirmConfidence  = 0;
FlushEvents;
mouseY = round(rand * (VASrange(2) - VASrange(1)) + VASrange(1));   %start at random position

startVAS = GetSecs;

while (GetSecs - startVAS) < cfg.maxConfRT && ~confirmConfidence 
    
    if strcmp(cfg.inputVAS, 'mouse')
        [mouseX, mouseY, mouseButtons] = GetMouse;
        confirmConfidence = mouseButtons(cfg.confirmConfidenceKey);
    elseif strcmp(cfg.inputVAS, 'gamepad')
        thumbstickValue = Gamepad('GetAxis', dev.gamepadIndex, cfg.VASaxisIndex);
        if thumbstickValue > dev.gamepadYposZero + 100  %add 100 to prevent it from moving alone (not sure why this happens)
            mouseY = mouseY + cfg.mouseStep;
        elseif thumbstickValue < dev.gamepadYnegZero -100
            mouseY = mouseY - cfg.mouseStep;
        end
        confirmConfidence = Gamepad('GetButton', dev.gamepadIndex, cfg.confirmConfidenceKey);
    end
    
    %restrict the bounds
    if mouseY > VASrange(2)
        mouseY = VASrange(2);
    elseif mouseY < VASrange(1)
        mouseY = VASrange(1);
    end
    
    %update display
    drawVAS(DATA);
    
    %draw the cursor centered on the mouse y-position
    Screen('DrawLine', dev.wnd, 255 * [1 0 0], DATA.centreRightAdjusted - cfg.VAS.bandWidth, mouseY, DATA.centreRightAdjusted + cfg.VAS.bandWidth, mouseY, 4);
    Screen('Flip',dev.wnd);
    
    if mouseY ~= initialy && ~movementBegun
        DATA.VAS(block,trial).firstRT = GetSecs - startVAS;
        movementBegun = 1;
    end
    
    mousetrail_y(counter)   = mouseY; 
    moveTime(counter)       = GetSecs - startVAS;
    counter                 = counter + 1;
end

if confirmConfidence
    DATA.VAS(block,trial).confidence      = (VASrange(2) - mouseY)/range(VASrange); 
    DATA.VAS(block,trial).conf_absolute   = mouseY;
    DATA.VAS(block,trial).movRT           = GetSecs - startVAS;
else
    DATA.VAS(block,trial).confidence    = NaN;
    DATA.VAS(block,trial).conf_absolute = NaN;
    DATA.VAS(block,trial).movRT         = NaN;
end

if ~movementBegun
    DATA.VAS(block,trial).firstRT      = NaN;
end

DATA.VAS(block,trial).mousetrail_y = mousetrail_y;
DATA.VAS(block,trial).moveTime     = moveTime;
Screen('Flip', dev.wnd);
WaitSecs(.3); %this prevents double clicks from messing it all up

end


function VASrange = drawVAS(DATA)

global dev;
global cfg;

%Bands should be 4 x n matrices with the rows corresponding to: left
%edge, top edge, right edge, bottom edge.
topEdge   = dev.centreScreenY + ( (0:(cfg.VAS.nBands -1)) - (cfg.VAS.nBands/2) ) * (cfg.VAS.bandLength);
lowEdge   = dev.centreScreenY + ( (1:(cfg.VAS.nBands   )) - (cfg.VAS.nBands/2) ) * (cfg.VAS.bandLength);
leftEdge  = repmat(DATA.centreRightAdjusted - cfg.VAS.bandWidth/2, 1, cfg.VAS.nBands);
rightEdge = repmat(DATA.centreRightAdjusted + cfg.VAS.bandWidth/2, 1, cfg.VAS.nBands);

bands  = [leftEdge; topEdge; rightEdge; lowEdge];
colors = repmat([cfg.VAS.colorDark' cfg.VAS.colorLight'], 1, cfg.VAS.nBands/2);

VASrange = [min(topEdge) max(lowEdge)];

Screen('TextSize', dev.wnd, 24);
Screen('FillRect', dev.wnd, colors, bands);
TextWidth = Screen('TextBounds', dev.wnd, cfg.VAS.questionString);
DrawFormattedText(dev.wnd, cfg.VAS.questionString, DATA.centreRightAdjusted - (TextWidth(3)/2), VASrange(1) - 80, cfg.txtCol);
TextWidth = Screen('TextBounds', dev.wnd, cfg.VAS.stringHigh);
DrawFormattedText(dev.wnd, cfg.VAS.stringHigh, DATA.centreRightAdjusted - (TextWidth(3)/2), VASrange(1) - TextWidth(4), cfg.txtCol);
TextWidth = Screen('TextBounds', dev.wnd, cfg.VAS.stringLow);
DrawFormattedText(dev.wnd, cfg.VAS.stringLow,  DATA.centreRightAdjusted - (TextWidth(3)/2), VASrange(2) + TextWidth(4), cfg.txtCol);

end