
function DATA = collectVASjudge(block, trial, DATA)

% 'Plugin' function to display and record responses to a visual analog
% scale. 
% 
% You'll have to set some configuration parameters in the function calling
% this one. They are:
% 
% Get mouseId
% dev.mouseId = GetMouseIndices;
% dev.mouseId = dev.mouseId(1);
% 
% [dev.wnd, dev.rect] = Screen('OpenWindow', screen, ... your OpenScreen
% command
%
% cfg.txtCol  = 255 * [1 1 1];
% dev.xCenter = (dev.rect(3) - dev.rect(1))/2 + dev.rect(1);
% dev.yCenter = (dev.rect(4) - dev.rect(2))/2 + dev.rect(2);
% 
% 
% cfg.maxConfRT          = Inf;            %secs
% cfg.VAS.bandWidth      = 20;
% cfg.VAS.stringLow      = 'Sehr unsicher';
% cfg.VAS.stringHigh     = 'Sehr sicher';
% cfg.VAS.questionString = 'Wie sicher?';
% cfg.VAS.nBands         = 10;
% cfg.VAS.bandLength     = round(dev.xCenter/7);     %width of each colour band in the confidence scale
% cfg.VAS.colorLight     = round(1/2 * 255 * [1 1 1]);
% cfg.VAS.colorDark      = 255 * [1 1 1];
%
% Data are stored in the DATA.VAS



global cfg;
global dev;

%draw display intially to get VASrange
VASrange = drawVAS;

%VASrange  = [dev.xCenter-100, dev.xCenter+100];
initialx  = VASrange(1) + (VASrange(2) - VASrange(1)) * rand;
initialx  = round(initialx);
constanty = 0; %We hide the actual cursor. Only the '|' sign on the scale is shown

SetMouse(initialx, 0, dev.wnd, dev.mouseId);
DATA.VAS(block,trial).mousePosOrig = (initialx - VASrange(1))/range(VASrange);
DATA.VAS(block,trial).onset = GetSecs;


%% Initialize

counter       = 1;
mousetrail_x  = [];
movementBegun = 0;
mouseButtons  = 0;
FlushEvents;

startVAS = GetSecs;

while (GetSecs - startVAS) < cfg.maxConfRT && ~mouseButtons(1)
    
    [mouseX, mouseY, mouseButtons] = GetMouse;
    if any(mouseButtons(2:length(mouseButtons)))        %TODO looks wrong
        DATA.VAS(block,trial).confidence      = (mouseX -VASrange(1))/range(VASrange); %TODO check that this is correct
        DATA.VAS(block,trial).conf_absolute   = mouseX;
        DATA.VAS(block,trial).movRT           = GetSecs - startVAS;
        break
    end
    
    %restrict the bounds
    if mouseX > VASrange(2)
        mouseX = VASrange(2);
    elseif mouseX < VASrange(1)
        mouseX = VASrange(1);
    end
    
    %update display
    VASrange = drawVAS;
    
    Screen('TextSize', dev.wnd, 64);                                    %draw the cursor
    TextWidth = Screen('TextBounds', dev.wnd, '|');                     %centered on the mouse x position
    correctionPixelsX = TextWidth(3) + 3;
    DrawFormattedText(dev.wnd, '|', mouseX - correctionPixelsX, dev.centreScreenY+20, 255 * [1 0 0]);
    Screen('Flip',dev.wnd);
    
    if mouseX ~= initialx && ~movementBegun
        DATA.VAS(block,trial).firstRT = GetSecs - startVAS;
        movementBegun = 1;
    end
    
    mousetrail_x(counter)   = mouseX - correctionPixelsX;
    moveTime(counter)       = GetSecs - startVAS;
    counter                 = counter + 1;
end

if mouseButtons(1)
    %internal to external extremes
    DATA.VAS(block,trial).confidence      = (mouseX - correctionPixelsX - VASrange(1))/range(VASrange);
    DATA.VAS(block,trial).conf_absolute   = mouseX - correctionPixelsX;
    DATA.VAS(block,trial).movRT           = GetSecs - startVAS;
else
    DATA.VAS(block,trial).confidence    = NaN;
    DATA.VAS(block,trial).conf_absolute = NaN;
    DATA.VAS(block,trial).movRT         = NaN;
end

if ~movementBegun
    DATA.VAS(block,trial).firstRT      = NaN;
end

DATA.VAS(block,trial).mousetrail_x = mousetrail_x;
DATA.VAS(block,trial).moveTime     = moveTime;
Screen('Flip', dev.wnd);
WaitSecs(.3); %this prevents double clicks from messing it all up

end


function VASrange = drawVAS

global dev;
global cfg;

%Bands should be 4 x n matrices with the rows corresponding to: left
%edge, top edge, right edge, bottom edge.
leftEdge  = dev.centreScreenX + ( (0:(cfg.VAS.nBands -1)) - (cfg.VAS.nBands/2) ) * (cfg.VAS.bandLength);
rightEdge = dev.centreScreenX + ( (1:(cfg.VAS.nBands   )) - (cfg.VAS.nBands/2) ) * (cfg.VAS.bandLength);
topEdge   = repmat(dev.centreScreenY - cfg.VAS.bandWidth/2, 1, cfg.VAS.nBands);
lowEdge   = repmat(dev.centreScreenY + cfg.VAS.bandWidth/2, 1, cfg.VAS.nBands);

bands  = [leftEdge; topEdge; rightEdge; lowEdge];
colors = repmat([cfg.VAS.colorDark' cfg.VAS.colorLight'], 1, cfg.VAS.nBands/2);

VASrange = [min(leftEdge) max(rightEdge)];

Screen('TextSize', dev.wnd, 24);
Screen('FillRect', dev.wnd, colors, bands);
DrawFormattedText(dev.wnd, cfg.VAS.questionString, 'center', dev.centreScreenY - 60, cfg.txtCol);
TextWidth = Screen('TextBounds', dev.wnd, cfg.VAS.stringHigh);
DrawFormattedText(dev.wnd, cfg.VAS.stringHigh, VASrange(2) - (TextWidth(3)/2), dev.centreScreenY + 40, cfg.txtCol);
TextWidth = Screen('TextBounds', dev.wnd, cfg.VAS.stringLow);
DrawFormattedText(dev.wnd, cfg.VAS.stringLow,  VASrange(1) - (TextWidth(3)/2), dev.centreScreenY + 40, cfg.txtCol);

end


