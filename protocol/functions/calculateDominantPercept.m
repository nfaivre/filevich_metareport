function DATA = calculateDominantPercept(b,t,DATA)

global cfg;

redCode = 3;    %get prime numbers, but anything would be fine really 
greenCode = 7; 

if ~isempty(DATA.continuousReport(b,t).keyIDpress)
    presses  = ones(1,length(DATA.continuousReport(b,t).keyPressTime));

    if ~isnan(DATA.continuousReport(b,t).keyIDrelease)
        releases = -1 * ones(1,length(DATA.continuousReport(b,t).keyReleaseTime));
    else
        releases = [];
    end
    allEvents = [presses, releases];
    allIDs    = [DATA.continuousReport(b,t).keyIDpress, DATA.continuousReport(b,t).keyIDrelease];
    [sortedEventTime, sortOrder] = sort([DATA.continuousReport(b,t).keyPressTime, DATA.continuousReport(b,t).keyReleaseTime]);
    sortedEventType = allEvents(sortOrder);
    sortedEventID   = allIDs(sortOrder);
    
    timeStep = .01;
    
    timeVector = 0 : timeStep : (DATA.trialDef.trialDuration(b,t)-timeStep);
    perceptVector = zeros(1,length(timeVector));                %initialize as 0s
    
    %And then crawl the event series and update the timeVector by adding
    %and subtracting from all subsequent events
    for event = 1:length(sortedEventType)
        if sortedEventID(event) == cfg.rivalryResponseKeyRed && sortedEventType(event) == 1 %red keypress
            perceptVector(timeVector >= sortedEventTime(event)) = perceptVector(timeVector >= sortedEventTime(event))  + redCode;
        elseif sortedEventID(event) == cfg.rivalryResponseKeyRed && sortedEventType(event) == -1 %red keyrelease
            perceptVector(timeVector >= sortedEventTime(event)) = perceptVector(timeVector >= sortedEventTime(event))  - redCode;
        elseif sortedEventID(event) == cfg.rivalryResponseKeyGreen && sortedEventType(event) == 1 %green keypress
            perceptVector(timeVector >= sortedEventTime(event)) = perceptVector(timeVector >= sortedEventTime(event))  + greenCode;
        elseif sortedEventID(event) == cfg.rivalryResponseKeyGreen && sortedEventType(event) == -1 %green keyrelease
            perceptVector(timeVector >= sortedEventTime(event)) = perceptVector(timeVector >= sortedEventTime(event))  - greenCode;
        end
    end
    
    timeRedOnly   = timeStep * sum(perceptVector == redCode);
    timeGreenOnly = timeStep * sum(perceptVector == greenCode);
    timeNone      = timeStep * sum(perceptVector == 0);
    timeBoth      = timeStep * sum(perceptVector == (redCode + greenCode));
        
    DATA.continuousReport(b,t).reportedDominance.greenSecs = timeGreenOnly;
    DATA.continuousReport(b,t).reportedDominance.redSecs   = timeRedOnly;
    DATA.continuousReport(b,t).reportedDominance.bothSecs  = timeBoth;
    DATA.continuousReport(b,t).reportedDominance.noneSecs  = timeNone;
    
end
end