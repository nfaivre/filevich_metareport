function [DATA, report] = collectContinuousReport(DATA, b, t, rivalryStimOnset, report)

global cfg;
global dev;

%Record keys pressed by quering the queue. This records only the first
%instance of each keyID, but it's safe to assume that a single key will
%be pressed only once within an iteration of this loop
[pressedKey, firstPress, firstRelease] = PsychHID('KbQueueCheck', dev.inputRivalryIndex);

%% Collect continuous report on rivalry 
if strcmp(cfg.inputRivalry, 'keyboard') || strcmp(cfg.inputRivalry, 'gamepad')
    if pressedKey
        pressedCodes = find(firstPress);
        for pressEvent = 1:size(pressedCodes,2)
            DATA.continuousReport(b,t).keyIDpress(report.keypressIndex + pressEvent)   = pressedCodes(pressEvent);
            DATA.continuousReport(b,t).keyPressTime(report.keypressIndex + pressEvent) = firstPress(pressedCodes(pressEvent)) - rivalryStimOnset;
        end
        %update
        report.keypressIndex = report.keypressIndex + pressEvent;
    end
    if any(firstRelease)
        releasedCodes = find(firstRelease);
        for releaseEvent = 1:size(releasedCodes,2)
            DATA.continuousReport(b,t).keyIDrelease(report.keyreleaseIndex  + releaseEvent)   = releasedCodes(releaseEvent);
            DATA.continuousReport(b,t).keyReleaseTime(report.keyreleaseIndex  + releaseEvent) = firstRelease(releasedCodes(releaseEvent)) - rivalryStimOnset;
        end
        %update
        report.keyreleaseIndex = report.keyreleaseIndex + releaseEvent;
    end
end

%% Collect responses about the attentional probes
if strcmp(cfg.inputProbe, 'keyboard')
    if pressedKey                       
        probeSeen = ismember(find(firstPress), [KbName(cfg.keyAttentionProbe1) KbName(cfg.keyAttentionProbe2)]);
        if any(probeSeen)
            DATA.continuousReport(b,t).attentionalProbeSeenID(report.probeIndex)   = probeSeen;
            if probeSeen == 1
                DATA.continuousReport(b,t).attentionalProbeSeenTime(report.probeIndex) = firstPress(KbName(cfg.keyAttentionProbe1));
            elseif probeSeen == 2
                DATA.continuousReport(b,t).attentionalProbeSeenTime(report.probeIndex) = firstPress(KbName(cfg.keyAttentionProbe2));
            end
            DATA.continuousReport(b,t).attentionalProbeSeenID(report.probeIndex) = probeSeen;
            %update
            %report.probeIndex = report.probeIndex + 1;
        end
    end
elseif  strcmp(cfg.inputRivalry, 'gamepad')
    thumbstickValue = Gamepad('GetAxis', dev.gamepadIndex, cfg.attentionProbeAxisIndex);
    % Line below not completely flexible. Make sure one is negative, the
    % other positive. You should be checking for smaller than the negative
    % and larger than the positive, not viceversa. 
    probeSeen = find([thumbstickValue < cfg.keyAttentionProbe1, thumbstickValue > cfg.keyAttentionProbe2]); 
    if ~isempty(probeSeen)
        DATA.continuousReport(b,t).attentionalProbeSeenTime(report.probeIndex) = GetSecs() - rivalryStimOnset; %TODO agree? Less accurate than with KbQueue, but oh well.
        DATA.continuousReport(b,t).attentionalProbeSeenID(report.probeIndex)   = probeSeen;        
        %report.probeIndex = report.probeIndex + 1;
    end
    
end

end