function DATA = collectType1Response(b, t, DATA)

global dev;
global cfg;

pressedKey = 0;
PsychHID('KbQueueStart', dev.inputRivalryIndex); 
DrawFormattedText(dev.wnd, cfg.type1Question, DATA.centreRightAdjusted, 'center', dev.white);
DATA.type1Question(b,t).presentationTime = Screen('Flip', dev.wnd);

%Wait for response
while ~any(pressedKey)
    if strcmp(cfg.inputType1, 'keyboard') || strcmp(cfg.inputType1, 'gamepad')
        [pressedKey, firstPress] = PsychHID('KbQueueCheck', dev.inputRivalryIndex); 
        %RestrictKeysForKbCheck doesn't work with KbQueue. So you have to
        %eliminate unwanted keys by hand: 
        if ~ismember(find(firstPress), [cfg.type1ResponseKeyRed cfg.type1ResponseKeyGreen])
            pressedKey = 0;
        end
    elseif strcmp(cfg.inputType1, 'mouse')
        [~, ~, pressedKey] = GetMouse;
    end
end

%Identify response
if strcmp(cfg.inputType1, 'keyboard')
    response = find(firstPress);
    responseTime = firstPress(firstPress>0);
elseif strcmp(cfg.inputType1, 'mouse')
    response = find(pressedKey);
    responseTime = GetSecs;
elseif strcmp(cfg.inputType1, 'gamepad')
    response = find(pressedKey);
    responseTime = GetSecs;
end

PsychHID('KbQueueStop', dev.inputRivalryIndex); 
PsychHID('KbQueueFlush', dev.inputRivalryIndex); 
DATA.type1Question(b,t).responseKeyID = response;
DATA.type1Question(b,t).responseTime  = responseTime - DATA.type1Question(b,t).presentationTime;

end


