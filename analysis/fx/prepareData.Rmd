

# Prepare data
```{r Prepare_data, include=FALSE}
rm(rawData)
# Read in
rawData <- read_csv('../../data/allsubjects.csv')

# Change variable names
colnames(rawData)[colnames(rawData)=="subject"]        <- "suj"
colnames(rawData)[colnames(rawData)=="trial"]          <- "trial"
colnames(rawData)[colnames(rawData)=="reportTrial"]    <- "contReport"
colnames(rawData)[colnames(rawData)=="type1Trial"]     <- "type1"
colnames(rawData)[colnames(rawData)=="longPercept"]    <- "stim"
colnames(rawData)[colnames(rawData)=="signal"]         <- "durationDifference"  #correct?
colnames(rawData)[colnames(rawData)=="noneSecs"]       <- "noneSecs"
colnames(rawData)[colnames(rawData)=="bothSecs"]       <- "bothSecs"
colnames(rawData)[colnames(rawData)=="greenSecs"]      <- "greenSecs"
colnames(rawData)[colnames(rawData)=="redSecs"]        <- "redSecs"
colnames(rawData)[colnames(rawData)=="greenSecs_full"] <- "greenSecs_full"
colnames(rawData)[colnames(rawData)=="redSecs_full"]   <- "redSecs_full"
colnames(rawData)[colnames(rawData)=="type1Response"]  <- "resp1"
colnames(rawData)[colnames(rawData)=="correct"]        <- "correct"
colnames(rawData)[colnames(rawData)=="type1RT"]        <- "type1RT"
colnames(rawData)[colnames(rawData)=="confidence"]     <- "conf"
colnames(rawData)[colnames(rawData)=="type1proxy"]     <- "predictedAnswer"
colnames(rawData)[colnames(rawData)=="goodProxy"]      <- "predictionCorrect"  #trials where Continuous report agrees with actual type 1 response

# Reencode factor levels and names
rawData$resp1[rawData$type1==0] = NaN
rawData$type1      = ifelse(rawData$type1==1,'R1+','R1-')
rawData$contReport = ifelse(rawData$contReport==1,'CR+','CR-')
rawData$contReport <- as.factor(rawData$contReport) 
rawData$type1      <- as.factor(rawData$type1) 
rawData$suj        <- as.factor(rawData$suj) 
rawData$proxyIsLongerPercept <- as.integer(rawData$proxyIsLongerPercept) #it's otherwise logical and glmer coplains (actually, sjplot does)
rawData$correctFactor <- as.factor(rawData$correct)
levels(rawData$correctFactor)[levels(rawData$correctFactor)=="0"] <- "Incorrect"
levels(rawData$correctFactor)[levels(rawData$correctFactor)=="1"] <- "Correct"

```

## Check which subjects should be excluded
```{r Check_Exclusion}
#First check if the pre-registered criteria are met

#Check accuracy in type 1 responses for each condition and subject separately
summaryData <- rawData %>%
  group_by(type1,contReport,suj) %>%
  dplyr::summarise(meanCorrectType1 = mean(correct))
any(summaryData$meanCorrectType1 < 0.6, na.rm = TRUE)
any(summaryData$meanCorrectType1 > 0.8, na.rm = TRUE)

#Plot staircases for each subject to examine them 
rawData$combinedConditions <- paste(rawData$contReport, rawData$type1, sep = " ")

ggplot(rawData, aes(x = trial, y = durationDifference, color=combinedConditions)) + 
  geom_line() +
  facet_wrap(~suj)

#exclude type1 RTs 
outOfRangeRT <- rawData$suj[which(rawData$type1RT < 0.2 | rawData$type1RT > 5)]
length(outOfRangeRT)
length(unique(outOfRangeRT))
rawData$type1RT[rawData$type1RT < 0.2] <- NA
rawData$type1RT[rawData$type1RT > 5] <- NA

#Check how many cancelled confidence ratings
which(rawData$type1 == "type1" & is.na(rawData$conf))
which(is.na(rawData$conf))

```

## Exclude Subjects
```{r Exclude_subjects}
# These subjects below have no contReport condition data!! 
badguys = c('HW140','NY151','PF_CH_004','PF_CH_006')
rawData = rawData %>% filter(!is.element(suj,badguys))
rawData$suj <- droplevels(rawData$suj)
```
