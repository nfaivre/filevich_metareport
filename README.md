**Contains experimental protocol, data, and analysis scripts for:**


Elisa Filevich, Christina Koß, and Nathan Faivre (2019). Response-related signals increase confidence but not metacognitive performance. biorXiv, doi:10.1101/735712v1


**Manuscript:** https://www.biorxiv.org/content/10.1101/735712v1  
**Preregistration:** https://osf.io/hnvsb/
